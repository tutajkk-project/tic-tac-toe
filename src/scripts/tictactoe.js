let imgbool = false
let win = 0
let p1win = 0
let p2win = 0
let fields = [
    [0,0,0],
    [0,0,0],
    [0,0,0],
]
let buttons = [
    [null,null,null],
    [null,null,null],
    [null,null,null],
]
let round = 1
let turn = 0

function loadButtons() {
    for (let i in buttons) {
        for (let j in buttons[i]) {
            let b = document.getElementById(`b${i}${j}`)
            buttons[i][j] = b
            b.addEventListener('click', buttonClicked)
        }
    }
}

function create_img() {
    if (imgbool == false){
        var img = document.createElement("img")
        img.src = "./assets/restart.png"
        img.id = "arrowvis"
        img.addEventListener('click',restart)
        document.body.appendChild(img) 
        imgbool = true
    }
    else{
        document.getElementById("arrowhide").id = 'arrowvis'
        document.getElementById("arrowvis").addEventListener('click',restart)
    }
}

function restart(){
    for (let row of buttons) {
        for (let btn of row) {
            btn.value = ""
            btn.className = "button"
        }
    }
    for (let j = 0; j <= 2; j++){
        for (let k = 0; k <= 2; k++){
            fields[j][k] = 0
        }
    }
    win = 0
    turn = 0
    round++
    document.getElementById("arrowvis").removeEventListener('click',restart)
    document.getElementById("rnd").innerHTML = `Round: ${round}`
    document.getElementById("arrowvis").id = 'arrowhide'
    document.getElementById("result").innerHTML = ""
}

function buttonClicked(e) {
    if (win == 0){
        const t = e.target

        if (t.value != "") {
            return
        }

        if (turn % 2 == 0) {
            t.value = 'O'
            fields [t.id[1]][t.id[2]] = 1
            document.getElementById("p1").innerHTML = `Player 1: O | Wins: ${p1win}`
            document.getElementById("p1").className = "player_act"
            document.getElementById("p2").innerHTML = `Player 2: X | Wins: ${p2win}`
            document.getElementById("p2").className = "player_inact"

        }
        else {
            t.value = 'X'
            fields [t.id[1]][t.id[2]] = 2
            document.getElementById("p1").innerHTML = `Player 1: O | Wins: ${p1win}`
            document.getElementById("p1").className = "player_inact"
            document.getElementById("p2").innerHTML = `Player 2: X | Wins: ${p2win}`
            document.getElementById("p2").className = "player_act"
        }
        turn++

        if (fields[t.id[1]][0] == fields[t.id[1]][1] && fields[t.id[1]][0] == fields[t.id[1]][2]) {
            win = fields[t.id[1]][0]
            for (let i = 0; i <= 2; i++){
                document.getElementById(`b${[t.id[1]]}${i}`).className = "victory"
            }
        }
        if (fields[0][t.id[2]] == fields[1][t.id[2]] && fields[0][t.id[2]] == fields[2][t.id[2]]) {
            win = fields[0][t.id[2]]
            for (let i = 0; i <= 2; i++){
                document.getElementById(`b${i}${[t.id[2]]}`).className = "victory"
            }
        }
        if (fields [0][0] == fields [1][1] && fields [0][0] == fields [2][2] && fields[0][0] > 0) {
            win = fields [0][0]
            for (let i = 0; i <= 2; i++){
                document.getElementById(`b${i}${i}`).className = "victory"
            }
        }
        if (fields [0][2] == fields [1][1] && fields [0][2] == fields [2][0] && fields[0][2] > 0) {
            win = fields [0][2]
            for (let i = 0, j = 2; i <= 2; i++, j--){
                document.getElementById(`b${i}${j}`).className = "victory"
            }
        }
        if (turn == 9 && win == 0){
                    document.getElementById("result").innerHTML = "Döntetlen!"
                    create_img()
        }
        if (win > 0){
            if (win == 1){
                p1win++
                document.getElementById("p1").innerHTML = `Player 1: O | Wins: ${p1win}`
                document.getElementById("result").innerHTML = `Nyertes: 1-es játékos!`
            }
            else{
                p2win++
                document.getElementById("result").innerHTML = `Nyertes: 2-es játékos!`
                document.getElementById("p2").innerHTML = `Player 2: X | Wins: ${p2win}`
            }
            create_img()
        }
    }
}
loadButtons()